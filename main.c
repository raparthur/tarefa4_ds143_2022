#include <stdio.h>
#include <stdlib.h>

typedef struct arvore {
 char info;
 struct arvore *esq;
 struct arvore *dir;
} Arvore;

Arvore* arv_constroi (char c, Arvore* e, Arvore* d) {
 Arvore* a = (Arvore*)malloc(sizeof(Arvore));
 a->info = c;
 a->esq = e;
 a->dir = d;
 return a;
}


int verifica_arv_vazia (Arvore* a) {
 return (a == NULL);
}


Arvore* arv_libera (Arvore* a) {
 if (!verifica_arv_vazia(a)) {
  arv_libera (a->esq);
  arv_libera (a->dir);
  free(a);
 }
 return NULL;
}

Arvore* cria_arv_vazia (void) {
 return NULL;
}

void pre_ordem(Arvore * a){

    if(!verifica_arv_vazia(a)){
       printf("%c,",a->info);
       if (!verifica_arv_vazia(a->dir) && !verifica_arv_vazia(a->esq)) {
           pre_ordem(a->esq);
           pre_ordem(a->dir);
       } else if (verifica_arv_vazia(a->dir) && !verifica_arv_vazia(a->esq)){
           pre_ordem(a->esq);
       } else if (!verifica_arv_vazia(a->dir) && verifica_arv_vazia(a->esq)){
           pre_ordem(a->dir);
       }

    }
}

void pre_ordem_cola(Arvore* a)
{
  if(!verifica_arv_vazia(a))
  {
    printf("%c,", a->info);
    pre_ordem_cola(a->esq);
    pre_ordem_cola(a->dir);
  }
}

void in_ordem_cola(Arvore * a){
   if(!verifica_arv_vazia(a))
  {
    in_ordem_cola(a->esq);
    printf("%c,", a->info);
    in_ordem_cola(a->dir);
  }
}

void pos_ordem(Arvore * a){

    if(!verifica_arv_vazia(a)){
       if (!verifica_arv_vazia(a->dir) && !verifica_arv_vazia(a->esq)) {
            pos_ordem(a->esq);
            pos_ordem(a->dir);
       } else if (verifica_arv_vazia(a->dir) && !verifica_arv_vazia(a->esq)){
           pos_ordem(a->esq);
       } else if (!verifica_arv_vazia(a->dir) && verifica_arv_vazia(a->esq)){
           pos_ordem(a->dir);
       }
        printf("%c,",a->info);

    }
}

void pos_ordem_cola(Arvore * a){
   if(!verifica_arv_vazia(a))
  {
    pos_ordem_cola(a->esq);
    pos_ordem_cola(a->dir);
    printf("%c,", a->info);
  }
}

void imprime1(Arvore * a){
    if(!verifica_arv_vazia(a)){
        if (!verifica_arv_vazia(a->dir)) {
            imprime1(a->dir);
        }
        if (!verifica_arv_vazia(a->esq)) {
            imprime1(a->esq);
        }
        printf("%c,",a->info);
    }
}

void imprime3(Arvore * a){

    if(!verifica_arv_vazia(a)){
       if (!verifica_arv_vazia(a->dir) && !verifica_arv_vazia(a->esq)) {
            imprime3(a->dir);
            imprime3(a->esq);
       } else if (verifica_arv_vazia(a->dir) && !verifica_arv_vazia(a->esq)){
           imprime3(a->esq);
       } else if (!verifica_arv_vazia(a->dir) && verifica_arv_vazia(a->esq)){
           imprime3(a->dir);
       }
        printf("%c,",a->info);

    }
}

void imprime4(Arvore * a){

    if(!verifica_arv_vazia(a)){
       printf("%c,",a->info);
       if (!verifica_arv_vazia(a->dir) && !verifica_arv_vazia(a->esq)) {
           imprime4(a->dir);
           imprime4(a->esq);
       } else if (verifica_arv_vazia(a->dir) && !verifica_arv_vazia(a->esq)){
           imprime4(a->esq);
       } else if (!verifica_arv_vazia(a->dir) && verifica_arv_vazia(a->esq)){
           imprime4(a->dir);
       }

    }
}


void imprime_notacao(Arvore * a){
    if(!verifica_arv_vazia(a)){
       printf("<");
       printf("%c",a->info);
       if (!verifica_arv_vazia(a->dir) && !verifica_arv_vazia(a->esq)) {
           imprime_notacao(a->esq);
           imprime_notacao(a->dir);
       } else if (verifica_arv_vazia(a->dir) && !verifica_arv_vazia(a->esq)){
           imprime_notacao(a->esq);
           printf("<>");
       } else if (!verifica_arv_vazia(a->dir) && verifica_arv_vazia(a->esq)){
           printf("<>");
           imprime_notacao(a->dir);
       } else {
           printf("<>");
           printf("<>");
       }
       printf(">");
    }
}

int arv_pertence_cola (Arvore *a, char c){
    if(verifica_arv_vazia(a)) { /* Se a �rvore estiver vazia, ent�o retorna 0 */
        return 0;
    }

  /* O operador l�gico || interrompe a busca quando o elemento for encontrado */
  return a->info==c || arv_pertence_cola(a->esq, c) || arv_pertence_cola(a->dir, c);
}

int conta_nos(Arvore a) {
    if(!verifica_arv_vazia(a.esq) && !verifica_arv_vazia(a.dir))
        return conta_nos(*a.esq) + conta_nos(*a.dir) + 1;
    else if(verifica_arv_vazia(a.esq) && !verifica_arv_vazia(a.dir))
        return conta_nos(*a.dir) + 1;
    else if(!verifica_arv_vazia(a.esq) && verifica_arv_vazia(a.dir))
        return conta_nos(*a.esq) + 1;
    else
        return 1;
}

//######################valor inteiro#########################
typedef struct arvore_int {
 int info;
 struct arvore_int *esq;
 struct arvore_int *dir;
} ArvoreInt;

ArvoreInt* arv_constroi_int (int c, ArvoreInt* e, ArvoreInt* d) {
 ArvoreInt* a = (ArvoreInt*)malloc(sizeof(ArvoreInt));
 a->info = c;
 a->esq = e;
 a->dir = d;
 return a;
}

ArvoreInt* cria_arv_vazia_int (void) {
 return NULL;
}

int verifica_arv_vazia_int (ArvoreInt* a) {
 return (a == NULL);
}

void pre_ordem_cola_int(ArvoreInt* a)
{
  if(!verifica_arv_vazia(a))
  {
    printf("%d,", a->info);
    pre_ordem_cola_int(a->esq);
    pre_ordem_cola_int(a->dir);
  }
}

int max_arvore(ArvoreInt a){
    if(a.dir == NULL || a.esq == NULL)
        return a.info;

    if(a.dir->info > a.esq->info)
        return max_arvore(*a.dir);
    else
        return max_arvore(*a.esq);
}
//##########################################################

int altura_arvore(Arvore *a){
    if(verifica_arv_vazia(a->dir) || verifica_arv_vazia(a->esq))
        return 0;

    int e = altura_arvore(a->esq);
    int d = altura_arvore(a->dir);
    if(e > d)
        return e + 1;
    else
        return d + 1;
}

int nos_folha_arvore(Arvore *a){
    if(!verifica_arv_vazia(a->dir) && !verifica_arv_vazia(a->esq))
        return nos_folha_arvore(a->dir) + nos_folha_arvore(a->esq);
    else if(verifica_arv_vazia(a->dir) && !verifica_arv_vazia(a->esq))
        return nos_folha_arvore(a->esq);
    else if(!verifica_arv_vazia(a->dir) && verifica_arv_vazia(a->esq))
        return nos_folha_arvore(a->dir);
    else
        return 1;
}

//########### TRABALHO ###########################
int eh_espelho(Arvore * arv_a, Arvore * arv_b){
    if(arv_a == NULL && arv_b == NULL){
        return 1;
    } else if((arv_a != NULL && arv_b == NULL) || (arv_a == NULL && arv_b != NULL)){
        return 0;
    } else if((arv_a->info != arv_b->info)){
        return 0;
    }

    if(eh_espelho(arv_a->dir,arv_b->esq) != 1 || eh_espelho(arv_a->esq,arv_b->dir) != 1){
        return 0;
    }
}

Arvore * cria_espelho(Arvore * arv_a){
    if(arv_a == NULL){
        return arv_a;
    }
    Arvore* arv_b = (Arvore*)malloc(sizeof(Arvore));
    arv_b->info = arv_a->info;

    //minha versao
    //arv_b->dir = arv_a->esq;
    //arv_b->esq = arv_a->dir;
    //cria_espelho(arv_a->dir);
    //cria_espelho(arv_a->esq);

    //cola da net
    arv_b->dir = cria_espelho(arv_a->esq);
    arv_b->esq = cria_espelho(arv_a->dir);

    return arv_b;
}

int main()
{

/*

    Arvore *a, *a1, *a2, *a3, *a4, *a5;
    a1 = arv_constroi('d',cria_arv_vazia(),cria_arv_vazia());
    a2 = arv_constroi('b',cria_arv_vazia(),a1);
    a3 = arv_constroi('e',cria_arv_vazia(),cria_arv_vazia());
    a4 = arv_constroi('f',cria_arv_vazia(),cria_arv_vazia());
    a5 = arv_constroi('c',a3,a4);
    a = arv_constroi('a',a2,a5);
*/
    Arvore *a = arv_constroi ('a',
            arv_constroi('b',
               cria_arv_vazia(),
               arv_constroi('d',cria_arv_vazia(),cria_arv_vazia())
                        ),
            arv_constroi('c',
               arv_constroi('e',cria_arv_vazia(),cria_arv_vazia()),
               arv_constroi('f',cria_arv_vazia(),cria_arv_vazia())
                        )
    );




    printf("------ 1 ----------");
    imprime1(a);

    printf("\n------ 3 ----------");
    imprime3(a);
    printf("\n------ 4 ----------");
    imprime4(a);

    printf("\n##########################\n");

    printf("\n--- exerc 1.1 e 3.2a [pre-ordem] --\n");
    pre_ordem(a);

    printf("\n---- exerc 1.2 --\n");
    imprime_notacao(a);

    printf("\n--- pertence c [internet]?-- (exerc 1.3)---\n");
    if(arv_pertence_cola(a,'c') == 1)
        printf("sim");
    else
        printf("nao");

    printf("\n----exerc 3.2a [pre ordem internet]------\n");
    pre_ordem_cola(a);

    printf("\n----exerc 3.2c [in ordem internet]------\n");
    in_ordem_cola(a);

    printf("\n----exerc 3.2c [pos ordem]------\n");
    pos_ordem(a);

    printf("\n----exerc 3.2c [pos ordem internet]------\n");
    pos_ordem_cola(a);

    printf("\n----qtd nos (3.3): %d ------\n",conta_nos(*a));



    printf("\n########### ARVORE INTEIRA #############\n");
    ArvoreInt *b = arv_constroi_int (1,
            arv_constroi_int(2,
               cria_arv_vazia_int(),
               arv_constroi_int(4,cria_arv_vazia_int(),cria_arv_vazia_int())
            ),
            arv_constroi_int(3,
               arv_constroi_int(5,cria_arv_vazia_int(),cria_arv_vazia_int()),
               arv_constroi_int(7,cria_arv_vazia_int(),
                            arv_constroi_int(6,cria_arv_vazia_int(),cria_arv_vazia_int()))
    )
 );

    printf("\n---- elementos ------\n");
    pre_ordem_cola_int(b);

    printf("\n----Maior (3.4): %d ------\n",max_arvore(*b));

    printf("\n########################################\n");

    printf("\n----Altura (3.5): %d ------\n",altura_arvore(a));

    printf("\n----Qtd folhas (3.6): %d ------\n",nos_folha_arvore(a));

    //==== espelhos ======
    Arvore *x = arv_constroi ('a',
            arv_constroi('b',
               arv_constroi('d',cria_arv_vazia(),cria_arv_vazia()),
               cria_arv_vazia()
            ),
            arv_constroi('c',
               arv_constroi('e',cria_arv_vazia(),cria_arv_vazia()),
               arv_constroi('f',cria_arv_vazia(),cria_arv_vazia())
            )
    );

    Arvore *y = arv_constroi ('a',
            arv_constroi('c',
               arv_constroi('f',cria_arv_vazia(),cria_arv_vazia()),
               arv_constroi('e',cria_arv_vazia(),cria_arv_vazia())
            ),
            arv_constroi('b',
               cria_arv_vazia(),
               arv_constroi('d',cria_arv_vazia(),cria_arv_vazia())
            )
    );
    //====================

    printf("\n########### TAREFA ################\n");
    printf("\n----pre ordem x ------\n");
    pre_ordem(x);
    printf("\n----pre ordem y ------\n");
    pre_ordem(y);
    printf("\n----Eh espelho(x,y): %d ------\n",eh_espelho(y,x));

    printf("\n----pre ordem x ------\n");
    pre_ordem(x);
    printf("\n----pre ordem espelho x ------\n");
    pre_ordem(cria_espelho(x));

    printf("\n----espelho funcionou: %d ------\n",eh_espelho(x,cria_espelho(x)));


    return 0;
}
